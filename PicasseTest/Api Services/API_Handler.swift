//
//  API_Handler.swift
//  Emcentrix
//
//  Created by Chaudhry Umair on 12/11/2020.
//  Copyright © 2020 Chaudhry Umair. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

class API_Handler: NSObject {
    
    static let instance : API_Handler = API_Handler()
    // MARK: Initializer
    private override init() {}
    public static func sharedManager() -> API_Handler {
        return instance
    }
    
   // MARK: List Api
    
    func getListFromService(completion: @escaping ([ListModel]?, Int) -> Void) {

        Alamofire.request(BASE_URL, method: .get, encoding: JSONEncoding.default).responseArray{ (response: DataResponse<[ListModel]>) in
            let data:[ListModel] = response.result.value ?? [ListModel] ()
               if response.response?.statusCode == 200 {
                   completion(data , response.response!.statusCode)
               }else {
                   completion(data , response.response!.statusCode)
               }
            }
        }
}
