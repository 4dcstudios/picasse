
//  NetworkConnectivity.swift
//  Parent Application
//
//  Created by Chaudhry Umair on 05/04/2020.
//  Copyright © 2020 Chaudhry Umair. All rights reserved.
//


import Foundation
import Alamofire

class NetworkConnectivity {
    
    
    class func isConnectedToInternet() -> Bool{
        return (NetworkReachabilityManager()?.isReachable)!
    }
    
}
