//
//  ListCell.swift
//  PicasseTest
//
//  Created by Chaudhry Umair on 11/01/2022.
//

import UIKit
import Kingfisher

class ListCell: UITableViewCell {
    // MARK: - IBOutlets
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnBuyNow: UIButton!
    // MARK: - cell functions

    override func awakeFromNib() {
        super.awakeFromNib()
        let view = UIView()
        view.backgroundColor = UIColor.white
        selectedBackgroundView = view
           
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    // MARK: - utility functions

    func setData(data: ListModel) {
        lblPrice.text = "Price : \(data.price ?? 0)"
        lblTitle.text = data.title
        lblCategory.text = data.category
        lblDescription.text = data.description
        let url = URL(string: data.image ?? "")
        imgProduct.kf.setImage(with: url)
          
    }
    
}
