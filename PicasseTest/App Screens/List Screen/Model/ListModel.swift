//
//  AppSettingModel.swift
//  emcentrix_New
//
//  Created by Chaudhry Umair on 29/12/2020.
//  Copyright © 2020 EmCentrix. All rights reserved.
//

import Foundation
import ObjectMapper

class ListModel : Mappable, Codable{
   
    var id: Int?
    var title: String?
    var price: Double?
    var description: String?
    var category: String?
    var image: String?
    var rate: Double?
    var count: Double?

    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        price <- map["price"]
        description <- map["description"]
        category <- map["category"]
        image <- map["image"]
        rate <- map["rating.rate"]
        count <- map["ratingcount"]

      }
    
}
