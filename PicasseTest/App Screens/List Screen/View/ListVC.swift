//
//  ViewController.swift
//  PicasseTest
//
//  Created by Chaudhry Umair on 10/01/2022.
//

import UIKit

class ListVC: BaseViewController {
    
    // MARK: IBOutlets
    @IBOutlet weak var lblTOtalPrice: UILabel!
    @IBOutlet weak var lblFetchingData: UILabel!
    @IBOutlet weak var listTable: UITableView!
    
    // MARK: Variables
    var listObject:[ListModel] = [ListModel]()
    var totalCost:Double = 0.0
    

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableviewSetup()
        getAllListService()
        lblTOtalPrice.text = "Total = 0.0"
    }
   
    
    // MARK: IBActions

    // MARK: Utility Functions
    func tableviewSetup() {
        listTable.delegate = self
        listTable.dataSource = self
        listTable.register(UINib(nibName: "ListCell", bundle: Bundle.main), forCellReuseIdentifier: "ListCell")
        listTable.estimatedRowHeight = 400
        listTable.isHidden = true
    }
    @objc func connected(sender: UIButton){
        totalCost += listObject[sender.tag].price ?? 0
        self.lblTOtalPrice.text = "Total is \(totalCost)"
    }

    }

    // MARK: - tableview
    extension ListVC: UITableViewDelegate , UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listObject.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
        cell.setData(data: listObject[indexPath.row])
        cell.viewSeparator.backgroundColor = .white
        cell.btnBuyNow.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        cell.btnBuyNow.tag = indexPath.row
        if indexPath.row % 2 == 0{
            cell.contentView.backgroundColor = .appBackColor
        }else {
            cell.contentView.backgroundColor = .accentColor
        }
        return cell
    }
        
       private func tableView(tableView: UITableView,heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            return UITableView.automaticDimension
             }
    }


// MARK: API Calls
extension ListVC{
    func getAllListService(){
        if !NetworkConnectivity.isConnectedToInternet(){
           // internet check
            let savedProductList = LocalStorageManager().getUser()
            if savedProductList.count > 0{
                showErrorWith(message: "No internet \(savedProductList.count) products available")
                self.listObject = savedProductList
                self.listTable.reloadData()
                listTable.isHidden = false
            }else{
                showErrorWith(message: "No internet \(savedProductList.count) products available")
                lblFetchingData.text = "No Data"
            }
           return}
        API_Handler.instance.getListFromService { list, statusCode in
            if statusCode == 200 {
                self.showSuccess(message: "Successfuly fetched \(list?.count ?? 0) products")
                self.listObject = list ?? [ListModel]()
                self.listTable.isHidden = false
                self.listTable.reloadData()
                LocalStorageManager().saveUser(user: self.listObject)
                
            }
            else{
                self.listTable.isHidden = true
                self.lblFetchingData.text = "No Data Fetched"
                self.showErrorWith(message: "Server error is encountered")
            }
        }
        
    }
}

