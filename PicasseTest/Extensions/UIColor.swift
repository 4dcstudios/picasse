//
//  UIColor.swift
//  Parent Application
//
//  Created by Monty Mobile on 07/05/2020.
//  Copyright © 2020 Chaudhry Umair. All rights reserved.
//

import UIKit


extension UIColor {
    
    static func applyTheme() {
           UINavigationBar.appearance().setBackgroundImage( primaryDarkColor.image() , for: .default)
           let attributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .regular) ]
           UINavigationBar.appearance().titleTextAttributes = attributes // Title fonts
           UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal)
           UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : accentColor ] // Title color
           UINavigationBar.appearance().tintColor = secondaryColor //  bar buttons
        
           UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for: .normal)

          UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.accentColor], for:.selected)
        
        UITabBar.appearance().tintColor = .accentColor
       }
    static var appBackColor: UIColor {
        
        return   UIColor(red: 21/255, green: 114/255, blue: 181/255, alpha: 1.0)
    }
    
    static var primaryColor: UIColor {
        
        return   .white
    }
    static var primaryDarkColor: UIColor {
        return   .white
    }
    static var accentColor: UIColor {
        return  UIColor(red: 134/255, green: 47/255, blue: 121/255, alpha: 1.0)
    }
    static var secondaryColor: UIColor {
        return   UIColor(red: 36/255, green: 42/255, blue: 56/255, alpha: 1.0)
    }
    
    func image(_ size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
    convenience init(hexString: String) {
           let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
           var int = UInt64()
           Scanner(string: hex).scanHexInt64(&int)
           let a, r, g, b: UInt64
           switch hex.count {
           case 3: // RGB (12-bit)
               (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
           case 6: // RGB (24-bit)
               (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
           case 8: // ARGB (32-bit)
               (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
           default:
               (a, r, g, b) = (255, 0, 0, 0)
           }
           self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
       }
}
