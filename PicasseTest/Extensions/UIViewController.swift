//
//  UIViewController.swift
//  Parent Application
//
//  Created by Monty Mobile on 06/05/2020.
//  Copyright © 2020 Rehan Tariq. All rights reserved.
//

import UIKit

enum AppStoryboard : String {
    case RegisterationStoryboard
    case DashboardStoryboard
    case TimeClockStoryboard
    case SettingsStoryboard
    case PTORequestStoryboard
    case CompanyHolidayStoryboard
    case PTORequestDetailsStoryboard
    case FIleCabinetStoryboard
    case KnwledgeBaseStoryboard
    case BenefitsSummaryStoryboard
    case MileageStoryboard
    case TimeSheetStoryboard
    case PayCheck

    

    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController >(viewControllerClass: T.Type) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return self.instance.instantiateViewController(withIdentifier:  storyboardID ) as! T
        
    }
}

extension UIViewController {
    
    class var storyboardID : String {
        return "\(self)"
    }
    
    static func instantiate( appStoryboard : AppStoryboard ) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }

}
