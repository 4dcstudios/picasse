
import UIKit
import Device
import NVActivityIndicatorView
import SwiftMessages
import SkeletonView
import Toaster
import Kingfisher
import ANLoader


protocol ACBaseViewControllerSearchDelegate {
    
    func searchWith(term: String)
    func cancelSearch()
}

class BaseViewController: UIViewController {

    
    var isColorBar = false
    var searchButton: UIBarButtonItem!
    var searchDelegate: ACBaseViewControllerSearchDelegate?
    
    var searchBar = UISearchBar()
    var leftSearchBarButtonItem : UIBarButtonItem?
    var rightSearchBarButtonItem : UIBarButtonItem?
    var cancelSearchBarButtonItem: UIBarButtonItem?
    var logoImageView : UIImageView!
    var controller : UIViewController?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
           navigationItem.setHidesBackButton(false, animated: false)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    func setupColorNavBar(){
        if let font = UIFont(name: "Oswald-Medium", size: 18){
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barStyle = .black
        }
        
        UIApplication.shared.statusBarStyle = .default
        navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        
        self.navigationController?.navigationBar.barTintColor = .red
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        //    UIColor(red: 189.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0)
        
        self.navigationItem.leftBarButtonItem = nil
    }
    
    func startLoader() {
       // ANLoader.showLoading("Loading", disableUI: true)

    }
    func stopLoader() {
      //  ANLoader.hide()

    }

    

    func setupTransparentNavBar(){
        if let font = UIFont(name: "Oswald-Medium", size: 18){
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor:UIColor.black]
            self.navigationController?.navigationBar.barStyle = .default
        }
        
        UIApplication.shared.statusBarStyle = .default
        navigationController?.navigationBar.isTranslucent = true
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        
        //        self.navigationController?.navigationBar.barTintColor = UIColor.red //UIColor(red: 189.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        //    UIColor(red: 189.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0)
        
        self.navigationItem.leftBarButtonItem = nil
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           navigationItem.setHidesBackButton(true, animated: false)
    }
    
    // convert image to binary
    
    func convertPNG (getIMage: UIImage)-> Data{
        let imageData = getIMage.pngData()
        return imageData!
    }
    
    
    func convertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
        }
        
    func convertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image!
    }
      
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    
    
    
    func addBackButtonToNavigationBar(){
        self.leftSearchBarButtonItem =  UIBarButtonItem(image: UIImage.init(named: "backbtn"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = self.leftSearchBarButtonItem;
    }
    func addCancelButtonToNavigationBar(text:String){
        self.leftSearchBarButtonItem =  UIBarButtonItem(title: text, style: UIBarButtonItem.Style.plain, target: self, action: #selector(goBack))
        if let font = UIFont(name: "Poppins", size: 11){
            self.leftSearchBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        }
        self.navigationItem.leftBarButtonItem = self.leftSearchBarButtonItem;
    }
   
   
    
    
//    func addMenuButtonToNavigationBar(){
//        self.leftSearchBarButtonItem =  UIBarButtonItem(image: UIImage.init(named: "side_menu"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(showSideMenu))
//        self.navigationItem.leftBarButtonItem = self.leftSearchBarButtonItem;
//    }
    
    func presentViewController(presented:UIViewController, presenting:UIViewController, isAnimated:Bool = true, presentationStyle:UIModalPresentationStyle = .fullScreen, completion: @escaping () -> ())
          {
              presented.modalPresentationStyle = presentationStyle
              presenting.present(presented, animated: isAnimated)
              {
                  completion()
              }
          }

    
    func addSearchButtonToNavigationBar(){
        self.rightSearchBarButtonItem =  UIBarButtonItem(image: UIImage.init(named: "search"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(doSearch))
        self.navigationItem.rightBarButtonItem = self.rightSearchBarButtonItem;
    }
    
    func addPlusButtonToNavigationBar(){
        self.rightSearchBarButtonItem =  UIBarButtonItem(image: UIImage.init(named: "plus"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(plusTapped))
        self.navigationItem.rightBarButtonItem = self.rightSearchBarButtonItem;
    }
  
    
 
    
    
    @objc func goBack(){
       
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @objc func doSearch(){
        self.showSearchbar()
    }
    @objc func plusTapped(){
        
        
    }
    
//    @objc func showSideMenu(){
//        self.sideMenuViewController.presentLeftMenuViewController()
//    }
    
    func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }


    func startSkeletonAnimation(){
        self.view.startSkeletonAnimation()
    }
    func stopSkeletonAnimation(){
        self.view.stopSkeletonAnimation()
    }

    func removeWhiteSpaces (text : String?)  -> String{
        return text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "No Data"

    }
    func removeSpaces (text : String?)  -> String{
           return text?.replacingOccurrences(of: " ", with: "") ?? "No Data"

       }
    
    func showErrorWith(message: String){
//        var config = SwiftMessages.Config()
//        config.presentationStyle = .bottom
        let error = MessageView.viewFromNib(layout: .tabView)
        error.configureTheme(.error)
        error.configureContent(title: "", body: message)
        //error.button?.setTitle("Stop", for: .normal)
        error.button?.isHidden = true
        
        SwiftMessages.show(view: error)
//        SwiftMessages.show(config: config, view: error)
    }
    
    
    func showSuccess(message: String){
               var config = SwiftMessages.Config()
        config.presentationStyle = .center
        let error = MessageView.viewFromNib(layout: .tabView)
        error.configureTheme(.success)
        error.configureContent(title: "", body: message)
        //error.button?.setTitle("Stop", for: .normal)
        error.button?.isHidden = true
        
        SwiftMessages.show(view: error)
        //        SwiftMessages.show(config: config, view: error)
    }
    
//    func alertBeta() {
//        let alert = UIAlertController(title: "Message", message: "will be implemented in beta", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
    
    
    
    func hideSearchBarAndMakeUIChanges(){
       
        navigationItem.setRightBarButton(self.rightSearchBarButtonItem, animated: true)
        navigationItem.setLeftBarButton(self.leftSearchBarButtonItem, animated: true)

        let navigationTitlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        navigationTitlelabel.center = CGPoint(x: 160, y: 284)
        navigationTitlelabel.textAlignment = NSTextAlignment.center
        navigationTitlelabel.textColor  = UIColor.white
        navigationTitlelabel.text = "Car Stock"
        
        if let font = UIFont(name: "Roboto-Light", size: 18){
            navigationTitlelabel.font = font
        }
        searchBar.text = ""
        
        self.navigationController!.navigationBar.topItem!.titleView = navigationTitlelabel
        
        
        self.searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar.alpha = 0;
        }, completion: { finished in
            self.searchBar.isHidden = true
        })
    }
    
    func hideSearchBar(){
        
        self.hideSearchBarAndMakeUIChanges()
    
    }
   
 
    
    func activateInitialSetupUI(){
    
        searchBar.delegate = self
        searchBar.placeholder = "Search"
        searchBar.searchBarStyle = .minimal
        searchBar.showsCancelButton = true
        
    }
    
    
    func showSearchbar(){
        searchBar.alpha = 0
        searchBar.isHidden = false
        searchBar.tintColor = UIColor.white
        searchBar.barTintColor = UIColor.white
         UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
        
        searchBar.setImage( UIImage.init(named: "search"), for: UISearchBar.Icon.search, state: UIControl.State.normal)
        

        navigationItem.titleView = searchBar
        navigationItem.setRightBarButton(nil, animated: true)
        navigationItem.setLeftBarButton(nil, animated: true)

        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar.alpha = 1;
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
    }
    
    
    
    
   
}

extension BaseViewController: UISearchBarDelegate{


    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        if(self.searchDelegate != nil){
            self.searchDelegate?.searchWith(term: searchBar.text!)
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        navigationItem.setHidesBackButton(true, animated: false)

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if(self.searchDelegate != nil){
            self.searchDelegate?.cancelSearch()
        }

        hideSearchBar()
    }
    func setShadow(view: UIView){
        let cornerRadius: CGFloat = 2
        let shadowOffsetWidth: Int = 0
        let shadowOffsetHeight: Int = 3
        let shadowColor = UIColor.gray.cgColor
        let shadowOpacity: Float = 0.5
        view.layer.cornerRadius = cornerRadius
        let shadowPath : UIBezierPath
    
    
        switch Device.size() {
            case .screen4Inch:
            shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: view.frame.size.width + 100, height: view.frame.size.height - 50), cornerRadius: cornerRadius)
            break
            case .screen4_7Inch:
    
            shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: view.frame.size.width + 100, height: view.frame.size.height - 15), cornerRadius: cornerRadius)
            break
            case .screen5_5Inch:
    
            shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: view.frame.size.width + 100, height: view.frame.size.height), cornerRadius: cornerRadius)
            break
            default:
            shadowPath = UIBezierPath(roundedRect: view.bounds, cornerRadius: cornerRadius)
            break
        }
    
        view.layer.masksToBounds = false
        view.layer.shadowColor = shadowColor
        view.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowPath = shadowPath.cgPath
    }
    
}
