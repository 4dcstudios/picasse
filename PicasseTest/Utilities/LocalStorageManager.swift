//
//  LocalStorageManager.swift
//  PicasseTest
//
//  Created by Chaudhry Umair on 11/01/2022.
//

import Foundation
 
class LocalStorageManager {
     
    public func saveUser(user: [ListModel]) {
        do {
             
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(user)
            let json = String(data: jsonData, encoding: .utf8) ?? "{}"
             
            let defaults: UserDefaults = UserDefaults.standard
            defaults.set(json, forKey: "user")
            defaults.synchronize()
            print("Saved in Offline Storage")
             
        } catch {
            print(error.localizedDescription)
        }
    }
     
    public func getUser() -> [ListModel] {
        do {
            if (UserDefaults.standard.object(forKey: "user") == nil) {
                return [ListModel]()
            } else {
                let json = UserDefaults.standard.string(forKey: "user") ?? "{}"
                 
                let jsonDecoder = JSONDecoder()
                guard let jsonData = json.data(using: .utf8) else {
                    return [ListModel]()
                }
                 
                let user = try jsonDecoder.decode([ListModel].self, from: jsonData)
                return user
            }
        } catch {
            print(error.localizedDescription)
        }
        return [ListModel]()
    }
     
    public func removeUser() {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.removeObject(forKey: "user")
        defaults.synchronize()
    }
}
