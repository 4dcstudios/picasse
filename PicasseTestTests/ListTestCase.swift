//
//  ListTestCase.swift
//  PicasseTestTests
//
//  Created by Chaudhry Umair on 31/01/2022.
//

import XCTest
@testable import PicasseTest

class ListTestCase: XCTestCase {

    func test_Url_Success(){
        let baseURLString = BASE_URL
        let expectedBaseURLString = "https://fakestoreapi.com/products"
        XCTAssertEqual(baseURLString, expectedBaseURLString)
    }
    
    func test_internet_Connectivity_isActive(){
        if NetworkConnectivity.isConnectedToInternet(){
            XCTAssertTrue(true)
        }
    }
    func test_internet_Connectivity_isNotActive(){
        if !NetworkConnectivity.isConnectedToInternet(){
            XCTAssertTrue(true)
        }
    }
    func test_Without_internet_first_Time_LocalStorage(){
        let savedProductList = LocalStorageManager().getUser()
        if savedProductList.count == 0{
            XCTAssertEqual(0 , savedProductList.count )
        }
    }
    
    func test_With_internet_first_Time_LocalStorage(){
        let savedProductList = LocalStorageManager().getUser()
        if savedProductList.count == 20{
            XCTAssertEqual(20 , savedProductList.count )
        }

    }
    
    
    
    
    
    

}
